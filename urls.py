from django.conf.urls import patterns, include, url
from django.contrib import admin
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
#admin.autodiscover()

urlpatterns = patterns('',
    # url(r'^$', 'apps.core.views.home', name='home'),
    url(r'^', include('apps.myapp.urls')),
    url(r'^api/', include('apps.api.urls')),
    url(r'^playas/', include('apps.playas.urls')),
    url(r'^users/', include('apps.users.urls')),

    # -------- ADMIN -----------
    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)