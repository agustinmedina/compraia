from django.contrib import admin
from apps.myapp.models import Marca, Vendedor, Pedido, Producto, Duenio, Cliente, MyUser
from apps.playas.models import Playa, Zona
# Register your models here.

admin.site.register(Zona)
admin.site.register(Producto)
admin.site.register(Duenio)
admin.site.register(Vendedor)
admin.site.register(Marca)
admin.site.register(Pedido)
admin.site.register(Cliente)
admin.site.register(Playa)
admin.site.register(MyUser)