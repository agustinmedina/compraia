import logging
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from apps.myapp.models import Marca, Producto, Pedido, Cliente, Duenio, Vendedor
from apps.playas.models import Playa, Zona
from apps.myapp.forms import MarcaForm, ProductoForm
from apps.users.forms import UserCreationFormVendedor

# Create your views here.

def home(request):
    return render (request,  "myapp/home.html")


def aboutus(request):
    return render (request, "myapp/aboutus.html")



def marcas(request):
    marcas = get_list_or_404(Marca)
    context = {'marcas':marcas}
    return render (request, "myapp/marcas.html", context)


def marca_detalle(request, marca_id):
   marca = get_object_or_404(Marca, id=marca_id)

   return render (request, "myapp/marca_detalle.html", {'marca':marca})


def producto_detalle(request, producto_id):
   producto = get_object_or_404(Producto, id=producto_id)

   return render (request, "myapp/producto_detalle.html", {'producto':producto})


@login_required
def create_marca(request):
    if request.method == 'POST':
        form = MarcaForm(request.POST or None)
        if form.is_valid(): # All validation rules pass
            user = request.user
            marca = form.save(commit=False)
            duenio = Duenio.objects.get(myuser_ptr_id=user.id)
            marca.duenio = duenio
            marca.save()
            return redirect(mi_marca)
    else:
        form = MarcaForm()
    return render(request, "myapp/crearmarca.html", {'form': form})


@login_required
def mi_marca(request):
    user = request.user
    duenio = Duenio.objects.get(myuser_ptr_id=user.id)
    marca = Marca.objects.get(duenio=duenio)
    vendedores = duenio.marca.vendedor_set.all()
    result = {}

    for v in vendedores:
         result[v] = v.producto_set.all()

    prods = Producto.objects.all()

    return render (request, "myapp/mimarca.html", {'vendedores':result, 'marca':marca})

@login_required
def edit_mimarca(request, marca_id):
    a_marca_instance = get_object_or_404(Marca, id=marca_id) 
    user = request.user
    duenio = get_object_or_404(Duenio, myuser_ptr_id=user.id)    
    if request.method == 'POST':    

        formeditmarca = MarcaForm(request.POST or None, instance= a_marca_instance)
        if formeditmarca.is_valid():
            formeditmarca.save()
            return redirect(reverse('mi_marca'))

        formnuevovendedor = UserCreationFormVendedor(request.POST)
        if formnuevovendedor.is_valid():
            vendedor = formnuevovendedor.save(commit=False)
            vendedor.marca = duenio.marca
            vendedor.save()
            return redirect(reverse('mi_marca'))

    else:
        formeditmarca = MarcaForm(request.POST or None, instance= a_marca_instance)
        formnuevovendedor = UserCreationFormVendedor(request.POST)

    return render(request, 'myapp/edit_mimarca.html', {'formeditmarca': formeditmarca, 'formnuevovendedor': formnuevovendedor})


@login_required
def add_producto(request, marca_id):
    a_marca_instance = get_object_or_404(Marca, id=marca_id) 
    if request.method == 'POST':    

        formnuevoproducto = ProductoForm(request.POST)
        if formnuevoproducto.is_valid():
            producto = formnuevoproducto.save(commit=False)
            producto.marca = a_marca_instance
            producto.save()
            return redirect(reverse('mi_marca'))

    else:
        formnuevoproducto = ProductoForm(request.POST)

    return render(request, 'myapp/add_producto.html', {'formnuevoproducto': formnuevoproducto})

@login_required
def edit_producto(request, marca_id, producto_id):
    producto = Producto.objects.get(id=producto_id) 
    if request.method == 'POST':    
        form = ProductoForm(request.POST or None, instance= producto)
        if form.is_valid():
            form.save()
            return redirect(reverse('mi_marca'))

    else:
        form = ProductoForm(request.POST or None, instance= producto)

    return render(request, 'myapp/edit_producto.html', {'form': form})