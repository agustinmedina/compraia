from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'apps.myapp.views.home', name='home'),
    url(r'^aboutus/?$', 'apps.myapp.views.aboutus', name='aboutus'),
    url(r'^marcas/?$', 'apps.myapp.views.marcas', name='lista_marcas'),
    url(r'^marcas/(?P<marca_id>\d+)/?$', 'apps.myapp.views.marca_detalle', name='marca_detalle'),
    url(r'^marcas/create/?$', 'apps.myapp.views.create_marca', name='marca_create'),
    url(r'^marcas/vendedores/(?P<vendedor_id>\d+)/?$', include('apps.users.urls')),
    url(r'^marcas/productos/(?P<producto_id>\d+)/?$', 'apps.myapp.views.producto_detalle', name='producto_detalle'),
    url(r'^mimarca/?$', 'apps.myapp.views.mi_marca', name='mi_marca'),
    url(r'^mimarca/edit/marca(?P<marca_id>\d+)/nuevovendedor/$', 'apps.users.views.add_vendedor', name='add_vendedor'),
    url(r'^mimarca/edit/marca(?P<marca_id>\d+)/$', 'apps.myapp.views.edit_mimarca', name='edit_mimarca'),
    url(r'^mimarca/edit/marca(?P<marca_id>\d+)/nuevoproducto/$', 'apps.myapp.views.add_producto', name='add_producto'),
    url(r'^mimarca/edit/marca(?P<marca_id>\d+)/prod(?P<producto_id>\d+)/$', 'apps.myapp.views.edit_producto', name='edit_producto'),
)