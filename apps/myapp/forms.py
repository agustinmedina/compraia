from apps.myapp.models import Marca, MyUser, Duenio, Vendedor, Producto, Pedido
from django.forms.widgets import Textarea
from django import forms


class MarcaForm(forms.ModelForm):
     class Meta:
        model = Marca
        fields = ('nombre', 'rubro', 'descripcion')


class ProductoForm(forms.ModelForm):

	class Meta:
		model = Producto
		fields = ('nombre', 'precio', 'vendedores',)

class PedidoForm(forms.ModelForm):

	class Meta:
		model = Pedido
		fields = ('vendedor', 'fecha') 