from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from apps.playas.models import Zona

# Create your models here.


class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
#            date_of_birth=date_of_birth,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
            password=password,
#            date_of_birth=date_of_birth
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    nombre = models.CharField(max_length=50)
    isduenio = models.BooleanField(default=False)
    email = models.EmailField(verbose_name='email address',
         max_length=255, unique=True)
#    date_of_birth = models.DateField(null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
#    REQUIRED_FIELD = ['date_of_birth']

    def is_duenio(self):
        return self.isduenio

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Duenio(MyUser):

    def __str__(self):
        return self.email


class Marca(models.Model):
    duenio = models.OneToOneField(Duenio)
    id = models.AutoField(verbose_name='ID', auto_created=True,
    serialize=False, primary_key=True)
    nombre = models.CharField(max_length=100)
    #??
    COMIDAS = 'Comidas'
    BEBIDAS = 'Bebidas'
    RUBROS_CHOICES = (
        (COMIDAS, 'Comidas'),
        (BEBIDAS, 'Bebidas'),
    )
    rubro = models.CharField(max_length=100, choices=RUBROS_CHOICES,
         blank=False)
    descripcion = models.TextField(max_length=40)

    def __str__(self):
        return self.nombre

    def get_prods(self):
        m = Marca.objects.get(nombre=self.nombre)
        productos = m.producto_set.all()
        return productos

    def to_dict(self):
        result = {
        "id": self.id,
        "duenio": self.duenio.email,
        "nombre": self.nombre,
        "rubro": self.rubro,
        "descripcion": self.descripcion
        }
        return result


class Cliente(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    def to_dict(self):
        result = {
        "id": self.id,
        "nombre": self.nombre,
        }
        return result


class Vendedor(MyUser):
    marca = models.ForeignKey(Marca)
    zona = models.ForeignKey(Zona)

    def __str__(self):
        return self.email

    def to_dict(self):
        result = {
        "id": self.id,
        "email": self.email,
        "marca": self.marca,
        "zona": self.zona
        }
        return result


class Pedido(models.Model):
    cliente = models.OneToOneField(Cliente)
    vendedor = models.ForeignKey(Vendedor)
    fecha = models.DateTimeField('fecha del pedido')

    def __str__(self):
        return self.cliente.nombre

    def to_dict(self):
        result = {
        "id": self.id,
        "cliente": self.cliente,
        "vendedor": self.vendedor,
        "fecha": self.fecha
        }
        return result


class Producto(models.Model):
    marca = models.ForeignKey(Marca)
    pedido = models.ForeignKey(Pedido, blank=True, null=True)
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField(max_length=100)
    vendedores = models.ManyToManyField(Vendedor)

    def __str__(self):
        return self.nombre

    def to_dict(self):
        result = {
        "id": self.id,
        "pedido": self.pedido,
        "nombre": self.nombre,
        "precio": self.precio,
        "vendedores": self.vendedores
        }
        return result