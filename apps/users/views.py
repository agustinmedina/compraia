from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404
from apps.myapp.models import Marca, Producto, Pedido, Cliente, Duenio, Vendedor
from apps.playas.models import Playa, Zona
from django.contrib.auth.decorators import login_required   
from apps.users.forms import UserCreationFormDuenio, UserCreationFormVendedor, UserChangeFormDuenio, UserChangeFormVendedor, VendedorForm, DuenioForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


# Create your views here.

def vendedor_detalle(request, vendedor_id):
   vendedor = get_object_or_404(Vendedor, id=vendedor_id)

   return render (request, "myapp/vendedor_detalle.html", {'vendedor':vendedor})

def registrarduenio(request):
    if request.method == 'POST':
        form = UserCreationFormDuenio(request.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(email=request.POST['email'], password=request.POST['password1'])
            login(request, new_user)
            return HttpResponseRedirect(reverse('apps.myapp.views.home'))
    else:
        form = UserCreationFormDuenio()
    return render(request, 'registrar.html', {'form': form})

def registrarvendedor(request):
    if request.method == 'POST':
        form = UserCreationFormVendedor(request.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(email=request.POST['email'],password=request.POST['password1'])
            login(request, new_user)
            return HttpResponseRedirect(reverse('apps.myapp.views.home'))
    else:
        form = UserCreationFormVendedor()
    return render(request, 'registrar.html', {'form': form})

@login_required
def miperfil(request):
    return render (request, "users/miperfil.html")


@login_required
def edit_miperfil(request):
    user = request.user
  #  if user.is_duenio:   
    duenio = get_object_or_404(Duenio, myuser_ptr_id=user.id) 
    if request.method == 'POST':    
        form = DuenioForm(request.POST or None, instance= duenio)
        if form.is_valid():
            form.save()
            return render(request, 'myapp/home.html')
    else:
        form = DuenioForm(request.POST or None, instance= duenio)
    return render(request, 'users/edit_miperfil.html', {'form': form})
#    else:
#        vendedor = Vendedor.objects.get(myuser_ptr_id=user.id) 
 #       if request.method == 'POST':
  #          formeditvendedor = VendedorForm(request.POST or None, instance= vendedor)
   #         if formeditvendedor.is_valid():
    #            formeditvendedor.save()
     #           return redirect(reverse('home'))
      #  else:
       #     formeditvendedor = VendedorForm(request.POST or None, instance= vendedor)
        #    return render(request, 'users/edit_miperfil.html', {'form': formeditvendedor})


@login_required
def add_vendedor(request, marca_id):
    user = request.user
    duenio = get_object_or_404(Duenio, myuser_ptr_id=user.id)    
    if request.method == 'POST': 
        formnuevovendedor = UserCreationFormVendedor(request.POST)
        if formnuevovendedor.is_valid():
            vendedor = formnuevovendedor.save(commit=False)
            vendedor.marca = duenio.marca
            vendedor.save()
            return redirect(reverse('mi_marca'))
    else:
        formnuevovendedor = UserCreationFormVendedor(request.POST)

    return render(request, 'users/add_vendedor.html', {'formnuevovendedor': formnuevovendedor})


@login_required
def edit_vendedor(request, vendedor_id): 
    vendedor = get_object_or_404(Vendedor, myuser_ptr_id=vendedor_id) 
    if request.method == 'POST':    
        form = VendedorForm(request.POST or None, instance=vendedor)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('apps.myapp.views.mi_marca'))
    else:
        form = VendedorForm(request.POST or None, instance=vendedor)
        return render(request, 'users/edit_vendedor.html', {'form': form})
