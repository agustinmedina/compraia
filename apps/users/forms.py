from apps.myapp.models import Marca, MyUser, Duenio, Vendedor
from django.forms.widgets import Textarea
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class UserCreationFormDuenio(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Duenio
        fields = ('nombre', 'email',)
        isduenio=True

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationFormDuenio, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.isduenio=True
            user.save()
        return user


class UserChangeFormDuenio(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Duenio
        fields = ('nombre', 'email')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserChangeFormDuenio, self).save(commit=False)
        if commit:
            user.save()
        return user


class UserCreationFormVendedor(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Vendedor
        fields = ('nombre', 'email', 'zona',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationFormVendedor, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeFormVendedor(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Vendedor
        fields = ('nombre', 'email', 'zona', 'marca', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserChangeFormVendedor, self).save(commit=False)
        if commit:
            user.save()
        return user


class VendedorForm(forms.ModelForm):
    class Meta:
        model = Vendedor
        fields = ('nombre', 'email', 'zona')
        

class DuenioForm(forms.ModelForm):
    class Meta:
        model = Duenio
        fields = ('nombre', 'email')