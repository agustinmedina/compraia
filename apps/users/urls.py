from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
	url(r'^registrarduenio/?$', 'apps.users.views.registrarduenio', name='registrarduenio'),
    url(r'^registrarvendedor/?$', 'apps.users.views.registrarvendedor', name='registrarvendedor'),
	url(r'^login/', "django.contrib.auth.views.login", dict(template_name= 'login.html'), name="login"),
    url(r'^logout/', "django.contrib.auth.views.logout", {"next_page": "/"}, name="logout"),
    url(r'^(?P<vendedor_id>\d+)/$', 'apps.users.views.vendedor_detalle', name='vendedor_detalle'),
    url(r'^(?P<vendedor_id>\d+)/edit/$', 'apps.users.views.edit_vendedor', name='edit_vendedor'),
    url(r'^miperfil/$', 'apps.users.views.miperfil', name='miperfil'),
    url(r'^miperfil/edit/$', 'apps.users.views.edit_miperfil', name='edit_miperfil'),
)
