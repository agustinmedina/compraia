from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, get_list_or_404
from django.forms.models import model_to_dict

from apps.myapp.models import Marca, Producto

# Create your views here.

def api_marcas(request):
    marcas = Marca.objects.all()
    m = {}
    l =[]
    for m in marcas: 
    	m = m.to_dict() 
    	l.append(m)
    result = { "marcas": l }
    return JsonResponse(result)


def api_marca_detalle(request, marca_id):
    marca = get_object_or_404(Marca, id=marca_id)
    marca = Marca.to_dict(marca)
    return JsonResponse(marca)