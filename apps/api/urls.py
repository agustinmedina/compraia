from django.conf.urls import patterns, include, url
from apps.api import views

urlpatterns = patterns('',
    url(r'^marcas/?$', 'apps.api.views.api_marcas', name='api_marcas'),
    url(r'^marcas/(?P<marca_id>\d+)/$', 'apps.api.views.api_marca_detalle', name='api_marca_detalle'),
)