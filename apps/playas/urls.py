from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'apps.playas.views.playas', name='lista_playas'),
    url(r'^(?P<playa_id>\d+)/$', 'apps.playas.views.playa_detalle', name='playa_detalle'),
    url(r'^(?P<playa_id>\d+)/zonas/$', 'apps.playas.views.playa_zonas', name='playa_zonas'),
    url(r'^(?P<playa_id>\d+)/zonas/(?P<zona_id>\d+)/$', 'apps.playas.views.zona_detalle', name='zona_detalle'),
)