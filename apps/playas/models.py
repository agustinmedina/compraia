from django.db import models

# Create your models here.
class Playa(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre   

    def to_dict(self):
        result = {
        "id":self.id,
        "nombre": self.nombre,
        }
        return result     


class Zona(models.Model):
    playa = models.ForeignKey(Playa)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    def to_dict(self):
        result = {
        "id":self.id,
        "playa": self.playa,
        "nombre": self.nombre,
        }
        return result
