from apps.myapp.models import Marca, MyUser, Duenio, Vendedor
from apps.playas.models import Zona, Playa
from django.forms.widgets import Textarea
from django import forms


class PlayaForm(forms.ModelForm):
     class Playa:
        model = Playa
        fields = ('nombre')


class EditPlayaForm(forms.ModelForm):

	class Playa:
		model = Playa
		fields = ('nombre') 

class ZonaForm(forms.ModelForm):
     class Zona:
        model = Zona
        fields = ('nombre')


class EditZonaForm(forms.ModelForm):

	class Zona:
		model = Zona
		fields = ('nombre')    