from django.shortcuts import render
from apps.myapp.models import Marca, Producto, Pedido, Cliente, Duenio, Vendedor
from apps.playas.models import Playa, Zona
import logging
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required

# Create your views here.

def playas(request):
    playas = get_list_or_404(Playa)
    context = {'playas': playas}
    return render (request, "myapp/playas.html", context)


def playa_detalle(request, playa_id):
    playa = get_object_or_404(Playa, id=playa_id)

    return render (request, "myapp/playa_detalle.html", {'playa':playa})

def playa_zonas(request, playa_id):
	playa = get_object_or_404(Playa, id=playa_id)
	zonas = Zona.objects.filter(playa=playa)
	context = {'zonas': zonas}
	return render (request, "myapp/playa_zonas.html", context)


def zona_detalle(request, zona_id):
    zona = get_object_or_404(Zona, id=zona_id)

    return render (request, "myapp/zona_detalle.html", {'zona':zona})

#@login_required
#def crearzona(request):
#	user = request.user
 #   if user.is_admin:
  #      if request.method == 'POST':
   #         form = ZonaForm(request.POST)
    #        if form.is_valid():
     #           zona = form.save(commit=False)
#
 #               return HttpResponseRedirect(reverse('apps.myapp.views.home'))
  #      else:
   #         form = UserCreationFormDuenio()
    #    return render(request, 'registrar.html', {'form': form})
    #else