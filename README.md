#**Como surge el proyecto?**

La idea surgue a partir de que estaba haciendo el curso sobre Django en la empresa DevsAr, y queria hacer
una aplicación sobre todo para aprender e iniciarme en el desarrollo, pero que a su vez me pudiera servir
de algo. El curso comenzó en octubre, y como sabía que en enero me iba un tiempo a Brasil a la casa de un amigo y
que él estaba con un emprendimiento de vendedor ambulante, pensé en hacer algo relacionado. La realidad del proyecto quedó
muy lejos de lo que imaginé en mi cabeza, el tiempo y los conocimientos no me alcanzaron. El proyecto lo realicé en su mayoria 
en ese mes y medio que tuve desde el curso hasta que me fui, lo retomé hace unas semanas y la idea es continuarlo.

#**De que se trata el proyecto?**

El proyecto es medio loco, se trata de una aplicación que te permite comprar y vender en la playa mediante una aplicación
Android. La aplicación Django sería el back-end del sistema, que provee una API JSON para que se realice la comunicación
con la aplicación movil. 

Te dejo un enlace del diagrama de clases como para que entiendas las logica que pensé:

https://drive.google.com/file/d/0B6i_mksJzvL3bkNnS09DUlNvT2c/view?usp=sharing


###**Aplicación web**

Mediante la aplicación web los usuarios se registran, crean una marca, agregan vendedores a su marca,
crean productos y los asignan a sus vendedores, y administran las playas y las zonas donde van a trabajar sus empleados.

Todas estas funcionalidades están implementadas, el dueño es quién crea usuarios vendedores y se encarga de toda
la administración de su marca. La API JSON solo muestra una marca especifíca, y una lista de marcas.


###**Aplicación Móvil**

Mediante la aplicación móvil se realizan pedidos y se localiza la ubicación de los vendedores cercanos en la playa.

Lo único que implementé de la aplicación móvil es pasar un array json y que se muestre en un listview, y cuando
se seleccione un elemento de la lista se muestren en un activity nuevo mas detalles del objeto, que en este caso son marcas.


##**Como instalar el proyecto?**

1) Crear un virtualenv (python 3.4)

2) Dentro del virtualenv instalar las dependencias del proyecto.

    pip install -r requirements.txt
    
3) Iniciar la base de datos para el proyecto.

    python manage.py syncdb 
    
4) Aplicar todas las migraciones

    python manage.py migrate
    
   
##**Para que se puedan usar todas las funcionalidades es necesario:**

1) Crear un usuario administrador

    python manage.py createsuperuser

2) Ejecutar el servidor Django

    pytho manage.py runserver
    
3) Ingresar e iniciar sesión en la administracion de de Django

    http://localhost:8000/admin/
    
4) Crear mediante la interfaz gráfica primero una Playa y luego una Zona ya que es necesario para que luego de crear 
una marca, poder crear un vendedor y asociarlo a una zona. La idea es que en un principio los administradores creen las playas
y las zonas, y que estas luego sean utilizadas por los usuarios (en implementación todo esto serián limites geográficos
donde en una playa pueden haber varias zonas).
